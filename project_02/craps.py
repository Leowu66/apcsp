import random

#roll the dice
def throw_dice():
   
    _ = input("press enter to roll the dice ")
     
    die1 = random.randrange(1, 7)
    die2 = random.randrange(1, 7)
     
    return (die1, die2) 


# get the dice sum
def dice_sum(dices):
    die1, die2 = dices
    print("You rolled {} + {} = {}".format(die1, die2, sum(dices)))


value = throw_dice()
dice_sum(value)
 
# find the sum
sum_of_dice = sum(value)

if sum_of_dice in (7, 11):
    result = "You win"


elif sum_of_dice in (2, 3, 12):
    result = "You lose"
     
#the third possibiliy play again 
else: 
    result = "Roll again"
    point = sum_of_dice
    print("Point is {}".format(point))
 
 
#game continue if you play again
while result == "Roll again":
    value = throw_dice()
    dice_sum(value)
    sum_of_dice = sum(value)
     
    if sum_of_dice == point:
        result = "You win"
         
    elif sum_of_dice == 7:
        result = "You lose"
 

if result == "You win":
    print("You win")
else:
    print("You lose")