import random

word_list = ['apple', 'banana', 'cherry', 'orange', 'pear', 'peach', 'pitaya', 'plum', 'strawberry', 'watermelon']


def choose_word():
    word = random.choice(word_list)
    return word




def guess(word):
    word_length = "_" * len(word)
    guessed = False
    guessed_letter = []
    guessed_words = []
    tries = 6
    print(hangman(tries))
    print(word_length)
    while not guessed and tries > 0:
        inp = input("Enter a letter: ")
        if len(inp) == 1 and inp.isalpha():
            if inp in guessed_letter:
                print("Already guessed {}".format(inp))
            elif inp not in word:
                print("{} is wrong".format(inp))
                tries -= 1
                guessed_letter.append(inp)
            else:
                print("{} is correct".format(inp))
                guessed_letter.append(inp)
                length = list(word_length)
                indices = [ i for i,letter in enumerate(word) if letter == inp]
                for index in indices:
                    length[index] = inp
                word_length = "".join(length)
                if "_" not in word_length:
                    guessed = True
        elif len(inp) == len(word) and inp.isalpha():
            if inp in guessed_words:
                print("Already guessed the {}".format(inp))
            elif inp != word:
                print("{} is wrong".format(inp))
                tries -= 1
                guessed_words.append(inp)
            else:
                guessed = True
                word_length = word
        else:
            print("Invalid")
        print(hangman(tries))
        print(word_length)
        print("\n")
    if guessed:
        print("You are correct, you win")
    else:
        print("Sorry no more chance, you lose. The word is {}".format(word))
    




def hangman(tries):
    stages = [  """
                    ----------
                    |         |
                    |         O
                    |        \|/
                    |         |
                    |        / \
                    |
                    |         
                    -------------
                """,
                """
                    ----------
                    |         |
                    |         O
                    |        \|/
                    |         |
                    |        / 
                    |
                    |         
                    -------------
                """,
                """
                    ----------
                    |         |
                    |         O
                    |        \|/
                    |         |
                    |        
                    |
                    |         
                    -------------
                """,
                """
                     ----------
                    |         |
                    |         O
                    |        \|/
                    |         
                    |        
                    |
                    |         
                    -------------
                """,
                """
                    ----------
                    |         |
                    |         O
                    |        \|
                    |         
                    |        
                    |
                    |         
                    -------------
                """,
                """
                    ----------
                    |         |
                    |         O
                    |         |
                    |         
                    |        
                    |
                    |         
                    -------------
                """,
                """
                    ----------
                    |         |
                    |         O
                    |       
                    |         
                    |        
                    |
                    |         
                    -------------
                """,
                """ ----------
                    |         |
                    |         
                    |      
                    |        
                    |        
                    |
                    |         
                    -------------
                """  
    ]
    return stages[tries]



def main():
    word = choose_word()
    guess(word)
    while input("Play again?(y/n): ") == "y":
        word = choose_word()
        guess(word)

if __name__ == "__main__":
    main()