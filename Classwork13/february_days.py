user_years = int(input('Enter a year: '))
if user_years % 100 == 0 | user_years % 400 == 0:
    print(f"In {user_years} February has 29 days")
elif user_years % 100 != 0 | user_years % 4 == 0:
    print(f"In {user_years} February has 29 days")
else:
    print(f"In {user_years} February has 28 days")