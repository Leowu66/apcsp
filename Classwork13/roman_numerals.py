user_number = int(input("Enter a number (between 1 and 10, inclusive): "))
if user_number < 1 or user_number > 10:
    message = "Invalid input"
    print(message)
else:
    if user_number == 1:
        print('I')
    elif user_number == 2:
        print('II')
    elif user_number == 3:
        print('III')
    elif user_number == 4:
        print('IV')
    elif user_number == 5:
        print('V')
    elif user_number == 6:
        print('VI')
    elif user_number == 7:
        print('VII')
    elif user_number == 8:
        print('VIII')
    elif user_number == 9:
        print('IX')
    elif user_number == 10:
        print('X')
