user_month = int(input("Enter a month: "))
user_day = int(input("Enter a day: "))
user_year = int(input("Enter a year(XX): "))

if (user_month)*(user_day) == user_year:
    print(f"{user_month}/{user_day}/{user_year} is a magic date")
else:
    print(f"{user_month}/{user_day}/{user_year} is not a magic date")
