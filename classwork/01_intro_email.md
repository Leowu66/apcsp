**Due:** 2022/09/05

Write an email to Mr. Wells (Subject: `[APCSP] Classwork 01`). Answering the following questions. See the example below.
1. What is your Chinese name? (given name, then family name)
2. What is your English name?
3. What is your GitLab username?
4. What is your homeroom?
5. Why are you taking this class?
6. Do you have any previous programming experience? If so, what computer language(s) do you know? What projects have you worked on?
7. I learn best in classes where the teacher…


```
Subject: [APCSP] Classwork 01
Body:
Hi Mr. Jakob,

Below are my responses for the first assignment.

1. What is your Chinese name?
Chen Li

2. What is your English name?
James

3. What is your GitLab username?
jamesli42

4. What is your homeroom?
9A

5. Why are you taking this class?
I took computer science last year and thought it was really interesting.

6. Do you have any previous programming experience?
Yes, C. I made a Tetris clone and a todo list.

7. I learn best in classes where the teacher asks me questions.

Cheers,
James Li
```
