def player_input():
    input1 = input("Enter Player 1 choice (rock/paper/scissors): ")
    input2 = input("Enter Player 2 choice (rock/paper/scissors): ")
    if input1 in ['rock', 'paper', 'scissors'] and input2 in ['rock', 'paper', 'scissors']:
        return input1, input2
    else:
        print("Invalid choice")
        return None

def win_tie(input1, input2):
    if input1 == input2:
        print("Tie!")
    elif (input1 == 'rock' and input2 == 'scissors') or (input1 == 'scissors' and input2 == 'paper') or (input1 == 'paper' and input2 == 'rock'):
        print("Player 1 wins")
    else:
        print("Player 2 wins")

def main():
    while True:
        inputs = player_input()
        if inputs is not None:
            input1, input2 = inputs
            win_tie(input1, input2)
        else:
            break

        choice = input("Do you want to play again? (y/n): ")
        if choice != 'y':
            print("Thanks for playing!")
            break

main()
