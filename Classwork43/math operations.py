# doing calculation
def add(num1, num2):
    return num1 + num2

def subtract(num1, num2):
    return num1 - num2

def  multiply(num1, num2):
    return num1 * num2

def divide(num1, num2):
    if num2 == 0:
        print("Error: division by zero")
    else:
        return num1 / num2



# take choice from user
num1 = float(input("Enter the first number: "))
num2 = float(input("Enter the second number: "))
user_choice = input("Enter the operation: ")

if user_choice == 'addition':
    result = add(num1, num2)
elif user_choice == 'subtraction':
    result = subtract(num1, num2)
elif user_choice == 'multiplication':
    result = multiply(num1, num2)
elif user_choice == 'division':
    result = divide(num1, num2)
else:
    result =  None

if result is None:
    print("Invalid operation")
else:
    print(f"Result: {result}")



