from random import randint

magic_number = randint(0, 100)

guess = int(input("Enter your guess: "))


while guess != magic_number:
    if guess < magic_number:
        print("Your guess is too low")
        guess = int(input("Enter your guess: "))
    elif guess > magic_number:
        print("Your guess is too high")
        guess = int(input("Enter your guess: "))
     
guess == magic_number
print(f"Yes, the number is {magic_number}.")

