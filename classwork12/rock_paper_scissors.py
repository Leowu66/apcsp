import random

possible_actions = ["rock", "paper", "scissors"]
computer_action = random.choice(possible_actions)

ROCK_ACTION = 0
PAPER_ACTION = 1
SCISSORS_ACTION = 2

user_score = 0
comp_score = 0
while True:
   user_input = input("Enter a choice (rock[0], paper[1], scissors[2]): ")
   user_action = int(user_input)
   print(f"\nYou chose {user_action} and the computer chose {computer_action}")
   if user_action == computer_action:
      print("It's a tie!")
   elif user_action == ROCK_ACTION:
      if computer_action == "scissors":
         print("You win!")
         user_score = user_score + 1    
      else:
         print("You lose!")
         comp_score = comp_score + 1   
   elif user_input == PAPER_ACTION:
      if computer_action == "rock":
         print("You win!")
         user_score = user_score + 1  
      else:
         print("You lose!")
         comp_score = comp_score + 1 
   elif user_input == SCISSORS_ACTION:
      if computer_action == "paper":
         print("You win!")
         user_score = user_score + 1            
      else:
         print("You lose!")
         comp_score = comp_score + 1 
   print(f"user_score is {user_score}")
   print(f"comp_score is {comp_score}")     
   play_again = input("Play again? (y/n): ")
   if play_again != "y":
      break

      


