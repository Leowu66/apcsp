class Coffeeshop:
    def __init__(self, name):
        self.name = name
        self.drink = dict()


    def add_drink(self, name, price):
        self.drink[name] = price
    
    def remove_drink(self, name):
        if name in self.drink:
            del self.drink[name]
    
    def view_menu(self):
        print(f"Menu of {self.name}:")
        for drink, price in self.drink.items():
            print(f"{drink}: ${price}")


coffee_shop = Coffeeshop("Leo's Coffee Shop")

coffee_shop.add_drink("Black Coffee", 3.00)
coffee_shop.add_drink("Mocha", 4.00)
coffee_shop.add_drink("Americano", 3.00)

coffee_shop.view_menu()

coffee_shop.remove_drink("Americano")

coffee_shop.view_menu()
