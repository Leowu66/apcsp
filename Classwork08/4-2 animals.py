animals = ["cat", "leopard", "tiger"]
for animal in animals: 
    print(animal.title())
print("\n")
statement = f"{animals[0].title()} has four legs"
print(statement)

statement = f"{animals[1].title()} runs fast"
print(statement)
statement = f"{animals[2].title()} has sharp teeth"
print(statement)

print("All these have tails")