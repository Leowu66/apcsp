speed = int(input("What is the speed of the vehicle in kph?: "))
hours = int(input("How many hours has it traveled: "))

for hour in range(1, hours):
    distance = hour * speed
    print(f"{hour}\t{distance}")