calories_minute = 4.2

for minutes in range(10, 31, 5 ):
    calories_burned = minutes * calories_minute
    print(f"{minutes}\t{calories_burned}")