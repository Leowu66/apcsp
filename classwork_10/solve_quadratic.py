import cmath  # For sqrt()

# Prompt the user for a, b, and c

# Calculate the discriminant

# Discriminant is 0 and has one solution
# Discriminant is positive and has two solutions
# Discriminant is negative and there are no real solutions
num = int(input("Enter an number: "))
num2 = int(input("Enter another number: "))
num3 = int(input("Enter another number: "))
a = num
b = num2
c = num3

d = (b*2) - (4*a*c)

sol1 = (-b-cmath.sqrt(d))/(2*a)
sol2 = (-b+cmath.sqrt(d))/(2*a)

print(f'The solution are {sol1} and {sol2}')
