import random

possible_actions = ["rock", "paper", "scissors"]
computer_action = random.choice(possible_actions)

user_input = input("Enter one of these (scissors, rock, paper): ")

print(f"\nYou chose {user_input} and the computer chose {computer_action}")

if computer_action == user_input:
    print("It's a tie!")

elif user_input == "rock":
    if computer_action == "scissors":
        print("You win!")
    else :
        print("You lose!")

elif user_input == "paper":
    if computer_action == "rock":
        print("You win!")
    else :
        print("You lose!")

elif user_ainput == "scissors":
    if computer_action == "paper":
        print("You win!")
    else :
        print("You lose!")