def is_prime(number):
    """Determines if a number is prime."""
    sum = 0
    for number in range(0, number):
        flag = 0
        if number < 2:
            sum+=0
        for x in range(2, number):
            if number % x == 0:
                flag = 1
                break              
        if flag == 0:
            sum+=1
    print(sum)
number = 10000
print(is_prime(number))