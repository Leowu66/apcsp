name_birthdays = {
    'Albert Einstein': '1879/3/14',
    'Benjamin Franklin': '1706/01/17',
    'Ada Lovelace': '1815/12/10'
}

print("Welcome to the birthday lookup. We know the birthdays of: ")
for name in name_birthdays:
    print(name)

print("Who's birthday do you want to look up?")
name = input()

if name in name_birthdays:
        print(f"{name}'s birthday is {name_birthdays[name]}.")