rivers = {
    'Nile': 'Egypt',
    'Yangtze': 'China',
    'Rhine': 'Germany'
    }
for river, country in rivers.items():
	print("The " + river + " river runs through " + country + ".")
print('\n')
for river in rivers.keys():
	print(river)
print('\n')
for country in rivers.values():
	print(country)
    