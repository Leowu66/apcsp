def falling_distance(time):
    """to calculate the objects falling time"""
    gravity = 9.8
    distance = (1/2)*gravity*(time**2)
    return distance


print("Time\tDitance")
for number in range(1,11):
    print(number, "\t", falling_distance(number))