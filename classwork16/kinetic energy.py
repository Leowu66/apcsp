def kinetic_energy(mass, velocity):
    KE = (1/2)*mass*(velocity**2)
    return KE

user_input1 = int(input("Enter mass (kg): "))
user_input2 = int(input("Enter velocity (m/s): "))

print(f"{kinetic_energy(user_input1, user_input2)} N")