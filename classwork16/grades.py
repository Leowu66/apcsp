def calc_average(score1, score2, score3, score4,score5):
    average = (score1+score2+score3+score4+score5)/5
    return average

def determine_grade(score1, score2, score3, score4,score5):
    for score in (score1, score2, score3, score4,score5):
        if 100 > score >= 90:
            print("A")
        elif 90 > score >= 80:
            print("B")
        elif 80 > score >= 70:
            print("C")
        elif 70 > score >= 60:
            print("D")
        elif score < 60:
            print("F")

i1 = int(input("Enter score: "))
i2 = int(input("Enter score: "))
i3 = int(input("Enter score: "))
i4 = int(input("Enter score: "))
i5 = int(input("Enter score: "))

print(f"Average: {calc_average(i1, i2, i3, i4, i5)}")
print(determine_grade(i1, i2, i3, i4, i5))
