class SRPN:
    def __init__(self):
        self.stack = []

    def process_command(self, command):
        """Process a single command from the user."""
        # If an "=" is received, print zero always
        if command == 'd':
            self.print_stack()
        elif command == '+':
            self.perform_additon()
        elif command == '-':
            self.perform_subtraction()
        elif command == '*':
            self.perform_multiplication()
        elif command == '/':
            self.perform_division()
        elif command == '%':
            self.perform_find_remainder()
        elif command == '=':
            self.peek_stack()
        elif command == '^':
            self.perform_power()
        elif command.isdigit():
            self.push_to_stack(int(command))
        else:
            print(f"unrecognised operator or operand \"{command}\"")

    def push_to_stack(self, number):
        """Push a number to the stack, if the stack is not full."""
        if len(self.stack) <= 23:
            self.stack.append(number)
        else:
            print("Stack overflow.")

    def peek_stack(self):
        """Print the top of the stack."""
        if self.stack:
            print(self.stack[-1])
        else:
            print("Stack empty.")

    def print_stack(self):
        """Print the contents of the stack."""
        if self.stack:
            for number in reversed(self.stack):
                print(number)
        else:
            print("Stack empty.")

    def has_two_operands(self):
        if len(self.stack) >= 2:
            return True
        else:
            print("Stack underflow.")
            return False

    def perform_additon(self):
        if self.has_two_operands():
            operand1 = self.stack.pop()
            operand2 = self.stack.pop()
            result = operand1 + operand2
            if result <= 2147483647:
                self.push_to_stack(result)
            else:
                print("Result overflow.")

    def perform_subtraction(self):
        if self.has_two_operands():
            operand1 = self.stack.pop()
            operand2 = self.stack.pop()
            result = operand2 - operand1
            if result <= 2147483647:
                self.push_to_stack(result)
            else:
                print("Result overflow.")

    def perform_multiplication(self):
        if self.has_two_operands():
            operand1 = self.stack.pop()
            operand2 = self.stack.pop()
            result = operand1 * operand2
            if result <= 2147483647:
                self.push_to_stack(result)
            else:
                print("Result overflow.")

    def perform_division(self):
        if self.has_two_operands():
            operand1 = self.stack.pop()
            operand2 = self.stack.pop()
            if operand1 != 0:
                result = operand2 / operand1
                if result <= 2147483647:
                    self.push_to_stack(result)
                else:
                    print("Result overflow.")
            else: 
                print("Divide by 0.")
                self.push_to_stack(operand2)
                self.push_to_stack(operand1)

    def perform_find_remainder(self):
        if self.has_two_operands():
            operand1 = self.stack.pop()
            operand2 = self.stack.pop()
            if operand1 != 0:
                result = operand2 % operand1
                if result <= 2147483647:
                    self.push_to_stack(result)
                else:
                    print("Result overflow.")
            else:
                print("Modulo by 0.")
                self.push_to_stack(operand2)
                self.push_to_stack(operand1)

    def perform_power(self):
        if self.has_two_operands():
            operand1 = self.stack.pop()
            operand2 = self.stack.pop()
            result = operand2 ** operand1
            if result <= 2147483647:
                self.push_to_stack(result)
            else:
                print("Result overflow.")