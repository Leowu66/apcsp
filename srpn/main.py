from srpn import SRPN
import sys


def main():
    # Code to take input from the command line
    # This input is passed to the process_command method in srpn.py
    srpn = SRPN()
    try:
        # Keep on accepting input from the command-line
        while True:
            command = sys.stdin.readline().rstrip('\n')
            # Close on an End-of-file (EOF) (Ctrl-D on the terminal)
            if not command:
                # Exit code 0 for a graceful exit
                sys.exit(0)
            # Otherwise, (attempt to) process the command
            srpn.process_command(command)
    except KeyboardInterrupt:
        # Exit code 0 for a graceful exit
        sys.exit(0)


if __name__ == '__main__':
    main()
