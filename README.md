# AP Computer Science Principles

## Grade breakdown

| Activity         | % of grade |
|------------------|------------|
| Participation    | 15%        |
| Classwork        | 15%        |
| Unit Evaluation  | 20%        |
| Midterm Evaluation | 20%      |
| Final Evaluation | 20%        |
| Projects         | 10%        |
| Extra Credit     | ≤5%        |
